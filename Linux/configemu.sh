#!/bin/bash

# configemu.sh            :: initial config (if empty)
# configemu.sh --clobber  :: initial config (forced)
# configemu.sh --devmode  :: switch to dev mode

PRGAMIGA=~/prg/amiga

# locate FS-UAE data path
FSUAE=~/Documents/FS-UAE
if [ ! -d $FSUAE ]; then
	FSUAE=~/FS-UAE
	if [ ! -d $FSUAE ]; then
		echo Unable to locate FS-UAE data directory
		exit
	fi
fi

# check for our RDB maker
if [ ! -f $PRGAMIGA/mk500rdb ]; then
	echo RDB maker not found - use copyandbuild.sh first
fi

# check for our ADF maker
if [ ! -f $PRGAMIGA/mkadf ]; then
	echo ADF maker not found - user copyandbuild.sh first
fi

# files - config, hard drive, and a temp floppy
FILE_CFG=$FSUAE/Configurations/Default.fs-uae
FILE_HDF=$FSUAE/Hard\ Drives/AmigaOS32.hdf
FILE_ADF=$FSUAE/Floppies/temp.adf

# option
INITIAL=0
CLOBBER=1
DEVMODE=2
OPTION=$INITIAL
if [ $# -ne 0 ]; then
	if [ "$1" = "--devmode" ]; then
		OPTION=$DEVMODE
	fi
	if [ "$1" = "--clobber" ]; then
		OPTION=$CLOBBER
	fi
fi

# safety check
if [ $OPTION -eq $INITIAL ]; then
	if [ -f $FILE_CFG ]; then
		echo existing configuration found
		echo use --clobber to wipe and start again from scratch
		echo or --devmode to switch to development config
		exit
	else
		OPTION=$CLOBBER
	fi
fi

# initial config
if [ $OPTION -eq $CLOBBER ]; then
	# build some files
	$PRGAMIGA/mk500rdb "$FILE_HDF"
	$PRGAMIGA/mkadf $FILE_ADF
	# build config file
	cat <<EOF >$FILE_CFG
[fs-uae]
amiga_model = A1200
chip_memry = 2048
fast_memory = 8192
joystick_port_1 = nothing
hard_drive_0 = AmigaOS32.hdf
hard_drive_0_controller = ide0
kickstart_file = kicka1200.rom
save_disk = 0
floppy_drive_volume = 0
floppy_image_0 = os32/install3.2.adf
floppy_image_1 = os32/workbench3.2.adf
floppy_image_2 = os32/diskdoctor.adf
floppy_image_3 = os32/locale.adf
floppy_image_4 = os32/locale-en.adf
floppy_image_5 = os32/extras3.2.adf
floppy_image_6 = os32/classes3.2.adf
floppy_image_7 = os32/fonts.adf
floppy_image_8 = os32/storage3.2.adf
floppy_image_9 = os32/backdrops3.2.adf
EOF
fi

# devmode change - strip floppy info, add temp floppy (writable), add xfer
# we must not duplicate any lines we add so strip and put back
if [ $OPTION -eq $DEVMODE ]; then
	if [ ! -f $FILE_CFG ]; then
		echo no existing config to switch to dev mode
		exit
	fi
	cat <<EOF >$FILE_CFG.new
$(grep -v -E 'floppy_image|hard_drive_1' $FILE_CFG)
floppy_image_0 = temp.adf
writable_floppy_images = 1
hard_drive_2 = AmigaDev_XFER
hard_drive_2_label = XFER
EOF
	mv $FILE_CFG.new $FILE_CFG
fi

