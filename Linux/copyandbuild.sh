#!/bin/bash

PRGAMIGA=~/prg/amiga

mkdir -p $PRGAMIGA
cp *.c $PRGAMIGA

cd $PRGAMIGA

gcc -Wall mk500rdb.c -o mk500rdb
gcc -Wall mkadf.c -o mkadf

