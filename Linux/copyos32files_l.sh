#!/bin/bash

# copyos32files_l.sh - Linux seems to read CD with lowercase names
# copyos32files_w.sh - Windows seems to reads CD with mixed case

# for case sensitivity
if [ "$0" = "copyos32files_l.sh" ]; then
	LOWER=1
	ROM="rom"
	ADF="adf"
else
	LOWER=0
	ROM="ROM"
	ADF="ADF"
fi

# default source path - AmigaOS 3.2 CD auto mounted by Raspberry Pi OS desktop
# (just in case you have a self-powered USB optical drive
SOURCEPATH=/media/pi/AmigaOS3.2CD

# destination path - version built from source seems to act differently...
DESTPATH=~/Documents/FS-UAE
if [ ! -d $DESTPATH ]; then
	DESTPATH=~/FS-UAE
	if [ ! -d $DESTPATH ]; then
		echo Unable to locate FS-UAE data directory
		exit
	fi
fi
echo "dest path ok"

# the ROM we want
ROMFILE=kicka1200.rom

# we want all these ADF files - allow for case sensitivity
if [ $LOWER -eq 1 ]; then
	ADFLIST="install3.2 workbench3.2 diskdoctor locale locale-en extras3.2 classes3.2 fonts storage3.2 backdrops3.2"
else
	ADFLIST="Install3.2 Workbench3.2 DiskDoctor Locale Locale-EN Extras3.2 Classes3.2 Fonts Storage3.2 Backdrops3.2"
fi

# check variable
BADSOURCE=0


# # # # PART 1 - PATH CHECK # # # #

# did user specify a path?
if [ $# -ne 0 ]; then
	SOURCEPATH=$1
fi

# check if path exists
if [ -d $SOURCEPATH ]; then
	echo "source path ok"
	# check if we can see the ROM file
	if [ -f $SOURCEPATH/$ROM/$ROMFILE ]; then
		echo "rom ok"
		# check for all the ADF files
		for d in $ADFLIST
		do
			if [ ! -f $SOURCEPATH/$ADF/$d.adf ]; then
				# bad if ADF mising
				BADSOURCE=1
				echo Unable to find $d.adf
			fi
		done
	else
		# can't see the ROM file - bad
		BADSOURCE=1
	fi
else
	# path doesn't exist - bad
	BADSOURCE=1
fi

if [ $BADSOURCE -eq 1 ]; then
	exit
fi

# # # # PART 2 - FILE COPYING # # # #

# first the ROM file
cp -f $SOURCEPATH/$ROM/$ROMFILE $DESTPATH/Kickstarts/

# now each ADF file we want - copy as lowercase regardless of source
mkdir -p $DESTPATH/Floppies/os32
for d in $ADFLIST
do
	a=$(echo "$d" | tr '[A-Z]' '[a-z]')
	cp -f $SOURCEPATH/$ADF/$d.adf $DESTPATH/Floppies/os32/$a.adf
done

