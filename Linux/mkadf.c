/* mkadf.c */

#include <stdio.h>

const unsigned int adfsize = 880*1024;

int main (int argc, char *argv[])
{
  FILE *f;
  char zerobyte = '\0';
  if (argc != 2)
  {
    puts ("Usage: mkadf filename.adf");
    return 0;
  }
  f = fopen (argv[1], "wb+");
  if (f != NULL)
  {
    fseek (f, adfsize - 1, SEEK_SET);
    fwrite (&zerobyte, 1, 1, f);
    fclose (f);
  }
  return 0;
}

