# T81T Dev

To accompany t81t.dev blog site

The intention is this can be "cloned" into a "transfer" hard disk folder with

    cd ~/Documents/FS-UAE/Hard\ Drives/AmigaDev_XFER
    git clone https://gitlab.com/bhandscomb/t81t-dev.git

Example assumes Linux system with suitable FS-UAE folder structure already existing.
