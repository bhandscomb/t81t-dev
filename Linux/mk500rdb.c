/* mk500rdb.c */

#include <stdio.h>

const unsigned int hdfsize = 500*1024*1024;

int main (int argc, char *argv[])
{
  FILE *f;
  char tag[5] = "rdsk";
  if (argc != 2)
  {
    puts ("Usage: mk500rdb filename.hdf");
    return 0;
  }
  f = fopen (argv[1], "wb+");
  if (f != NULL)
  {
    fwrite (tag, 1, 4, f);
    fseek (f, hdfsize - 1, SEEK_SET);
    fwrite (&tag[4], 1, 1, f);
    fclose (f);
  }
  return 0;
}

